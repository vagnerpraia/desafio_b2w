package test

type Test struct {
	Path string
	BodyRequest string
	CodeExpected int
	BodyExpected string
}