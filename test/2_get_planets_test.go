package test

import (
	"net/http"
	"net/http/httptest"
	"regexp"
	"testing"

	"gitlab.com/vagnerpraia/desafio_b2w/internal/app/api/routes"
)

func TestGetPlanets(t *testing.T) {
	r := router.GetRouter()
	ts := httptest.NewServer(r)
	defer ts.Close()

	test1 := Test {
		Path: "/v1/planets",
		CodeExpected: 200,
		BodyExpected: `^({"message":")+([.])+(","result":[{)+([.])+(}]})$`,
	}

	test2 := Test {
		Path: "/v1/planets",
		CodeExpected: 500,
		BodyExpected: `^({"message":")+([.])+(","result":null})$`,
	}

	test3 := Test {
		Path: "/v1/planet",
		CodeExpected: 500,
		BodyExpected: `^({"message":")+([.])+(","result":null})$`,
	}

	testList := []Test{test1, test2, test3}

    for _, test := range testList {
		url := ts.URL + test.Path
		resp, _ := http.Get(url)

		codeResponse := resp.StatusCode
		if codeResponse != test.CodeExpected {
			t.Errorf(msgCode, test.CodeExpected, codeResponse)
		}

		bodyResponse := setBody(resp.Body)
		match, _ := regexp.MatchString(test.BodyExpected, bodyResponse)
		if match {
			t.Errorf(msgBody, test.BodyExpected, bodyResponse)
		}
	}
}