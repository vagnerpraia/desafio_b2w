package test

import (
	"io"
	"io/ioutil"
	"strings"
)

func setBody(body io.ReadCloser) (result string) {
	bodyAdjusted, err := ioutil.ReadAll(body)
	if err == nil {
		result = strings.TrimSpace(string(bodyAdjusted))
	}

	return
}