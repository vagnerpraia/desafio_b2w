package test

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"regexp"
	"testing"

	"gitlab.com/vagnerpraia/desafio_b2w/internal/app/api/routes"
)

func TestPostSearch(t *testing.T) {
	r := router.GetRouter()
	ts := httptest.NewServer(r)
	defer ts.Close()

	test1 := Test {
		Path: "/v1/search",
		BodyRequest: `{"name":"Test 1"}`,
		CodeExpected: 200,
		BodyExpected: `^({"message":")+([.])+(","result":[{"id":"1",)+([.])+(]})$`,
	}

	test2 := Test {
		Path: "/v1/search",
		BodyRequest: `{"name":"Test 2"}`,
		CodeExpected: 200,
		BodyExpected: `^({"message":")+([.])+(","result":[{"id":"2",)+([.])+(]})$`,
	}

	test3 := Test {
		Path: "/v1/search",
		BodyRequest: `{"name":"qwertyuiop"}`,
		CodeExpected: 204,
	}

	test4 := Test {
		Path: "/v1/search",
		BodyRequest: `{"name":""}`,
		CodeExpected: 403,
		BodyExpected: `^({"message":")+([.])+(","result":null})$`,
	}

	test5 := Test {
		Path: "/v1/searc",
		BodyRequest: `{"name":"Test 1"}`,
		CodeExpected: 500,
		BodyExpected: `^({"message":")+([.])+(","result":null})$`,
	}

    testList := []Test{test1, test2, test3, test4, test5}

    for _, test := range testList {
		url := ts.URL + test.Path
		search := bytes.NewBuffer([]byte(test.BodyRequest))
		resp, _ := http.Post(url, "application/json", search)

		codeResponse := resp.StatusCode
		if codeResponse != test.CodeExpected {
			t.Errorf(msgCode, test.CodeExpected, codeResponse)
		}

		bodyResponse := setBody(resp.Body)
		match, _ := regexp.MatchString(test.BodyExpected, bodyResponse)
		if match {
			t.Errorf(msgBody, test.BodyExpected, bodyResponse)
		}
	}
}