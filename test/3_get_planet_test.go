package test

import (
	"net/http"
	"net/http/httptest"
	"regexp"
	"testing"

	"gitlab.com/vagnerpraia/desafio_b2w/internal/app/api/routes"
)

func TestGetPlanet(t *testing.T) {
	r := router.GetRouter()
	ts := httptest.NewServer(r)
	defer ts.Close()

	test1 := Test {
		Path: "/v1/planets/1",
		CodeExpected: 200,
		BodyExpected: `^({"message":")+([.])+(","result":{"id":"1","name":"Test 1","climate":"Test 1","terrain":"Test 1","number_movies":1,"films":["Film A"],"url":")+([.])+(/v1/planets/1"}})$`,
	}

	test2 := Test {
		Path: "/v1/planets/2",
		CodeExpected: 200,
		BodyExpected: `^({"message":")+([.])+(","result":{"id":"2","name":"Test 2","climate":"Test 2","terrain":"Test 2","number_movies":2,"films":["Film A","Film B"],"url":")+([.])+(/v1/planets/2"}})$`,
	}

	test3 := Test {
		Path: "/v1/planet/99",
		CodeExpected: 204,
	}

	test4 := Test {
		Path: "/v1/planet/1",
		CodeExpected: 500,
		BodyExpected: `^({"message":")+([.])+(","result":null})$`,
	}

	testList := []Test{test1, test2, test3, test4}

    for _, test := range testList {
		url := ts.URL + test.Path
		resp, _ := http.Get(url)

		codeResponse := resp.StatusCode
		if codeResponse != test.CodeExpected {
			t.Errorf(msgCode, test.CodeExpected, codeResponse)
		}

		bodyResponse := setBody(resp.Body)
		match, _ := regexp.MatchString(test.BodyExpected, bodyResponse)
		if match {
			t.Errorf(msgBody, test.BodyExpected, bodyResponse)
		}
	}
}