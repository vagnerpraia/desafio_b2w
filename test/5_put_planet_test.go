package test

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"regexp"
	"testing"

	"gitlab.com/vagnerpraia/desafio_b2w/internal/app/api/routes"
)

func TestPutPlanet(t *testing.T) {
	r := router.GetRouter()
	ts := httptest.NewServer(r)
	defer ts.Close()

	test1 := Test {
		Path: "/planets",
		BodyRequest: `{"id":"1","name":"Test 1","climate":"Test 1","terrain":"Test 1","films":["Film A"]}`,
		CodeExpected: 200,
		BodyExpected: `^({"message":")+([.])+(","result":{"id":"1","name":"Test 1","climate":"Test 1","terrain":"Test 1","number_movies":1,"films":["Film A"],"url":")+([.])+(/planets/1"}})$`,
	}

	test2 := Test {
		Path: "/planets",
		BodyRequest: `{"id":"2","name":"Test 9","climate":"Test 9","terrain":"Test 9","films":["Film A","Film B","Film C"]}`,
		CodeExpected: 200,
		BodyExpected: `^({"message":")+([.])+(","result":{"id":"2","name":"Test 9","climate":"Test 9","terrain":"Test 9","number_movies":3,"films":["Film A","Film B","Film C"],"url":")+([.])+(/planets/2"}})$`,
	}

	test3 := Test {
		Path: "/planets",
		BodyRequest: `{"id":"2","name":"Test 2","climate":"Test 2","terrain":"Test 2","films":["Film A","Film B"]}`,
		CodeExpected: 200,
		BodyExpected: `^({"message":")+([.])+(","result":{"id":"2","name":"Test 2","climate":"Test 2","terrain":"Test 2","number_movies":2,"films":["Film A","Film B"],"url":")+([.])+(/planets/2"}})$`,
	}

	test4 := Test {
		Path: "/planets",
		BodyRequest: `{"id":"99","name":"","climate":"Test 99","terrain":"Test 99","films":["Film Z"]}`,
		CodeExpected: 403,
		BodyExpected: `^({"message":")+([.])+(","result":null})$`,
	}

	test5 := Test {
		Path: "/planets",
		BodyRequest: `{"id":"99","name":"Test 99","climate":"Test 99","terrain":"Test 99","films":["Film Z"]}`,
		CodeExpected: 500,
		BodyExpected: `^({"message":")+([.])+(","result":null})$`,
	}

	test6 := Test {
		Path: "/planet",
		BodyRequest: `{"id":"1","name":"Test 99","climate":"Test 99","terrain":"Test 99","films":["Film Z"]}`,
		CodeExpected: 500,
		BodyExpected: `^({"message":")+([.])+(","result":null})$`,
	}

	testList := []Test{test1, test2, test3, test4, test5, test6}

    for _, test := range testList {
		url := ts.URL + test.Path
		planet := bytes.NewBuffer([]byte(test.BodyRequest))
		req, _ := http.NewRequest(http.MethodPut, url, planet)
		resp := req.Response

		codeResponse := resp.StatusCode
		if codeResponse != test.CodeExpected {
			t.Errorf(msgCode, test.CodeExpected, codeResponse)
		}

		bodyResponse := setBody(resp.Body)
		match, _ := regexp.MatchString(test.BodyExpected, bodyResponse)
		if match {
			t.Errorf(msgBody, test.BodyExpected, bodyResponse)
		}
	}
}