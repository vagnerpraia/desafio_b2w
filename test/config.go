package test

import (
	"gitlab.com/vagnerpraia/desafio_b2w/internal/app/api/databases"
)

const (
	colletionName = "planet"
	msgCode = "\n\nCódigo diferente do esperado.\nEsperado: %v\nRetornado: %v\n\n"
	msgBody = "\n\nCorpo diferente do esperado.\nEsperado: %v\nRetornado: %v\n\n"
)

var db = databases.GetConnectionMongo()
var collection = db.C(colletionName)

func clearCollection() {
	collection.RemoveAll(nil)
}
