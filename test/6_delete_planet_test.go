package test

import (
	"net/http"
	"net/http/httptest"
	"regexp"
	"testing"

	"gitlab.com/vagnerpraia/desafio_b2w/internal/app/api/routes"
)

func TestDeletePlanet(t *testing.T) {
	r := router.GetRouter()
	ts := httptest.NewServer(r)
	defer ts.Close()

	test1 := Test {
		Path: "/v1/planets/1",
		BodyRequest: `{"id":"1","name":"Test 1","climate":"Test 1","terrain":"Test 1","films":["Film A"]}`,
		CodeExpected: 200,
		BodyExpected: `^({"message":")+([.])+(","result":null})$`,
	}

	test2 := Test {
		Path: "/v1/planets/2",
		CodeExpected: 200,
		BodyExpected: `^({"message":")+([.])+(","result":null})$`,
	}

	test3 := Test {
		Path: "/v1/planets",
		CodeExpected: 405,
	}

	test4 := Test {
		Path: "/v1/planets/99",
		CodeExpected: 500,
		BodyExpected: `^({"message":")+([.])+(","result":null})$`,
	}

	test5 := Test {
		Path: "/v1/planet/99",
		CodeExpected: 500,
		BodyExpected: `^({"message":")+([.])+(","result":null})$`,
	}

	testList := []Test{test1, test2, test3, test4, test5}

    for _, test := range testList {
		url := ts.URL + test.Path
		req, _ := http.NewRequest(http.MethodDelete, url, nil)
		resp := req.Response

		codeResponse := resp.StatusCode
		if codeResponse != test.CodeExpected {
			t.Errorf(msgCode, test.CodeExpected, codeResponse)
		}

		bodyResponse := setBody(resp.Body)
		match, _ := regexp.MatchString(test.BodyExpected, bodyResponse)
		if match {
			t.Errorf(msgBody, test.BodyExpected, bodyResponse)
		}
	}
}