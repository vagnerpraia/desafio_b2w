package creator_script

type Response struct {
	Count int `json:"count" bson:"count"`
    Next string `json:"next" bson:"next"`
    Previous string `json:"previous" bson:"previous"`
	Results []Planet `json:"results" bson:"results"`
}

type Planet struct {
	Id string `json:"_id" bson:"_id"`
	Name string `json:"name" bson:"name"`
	Climate string `json:"climate" bson:"climate"`
	Terrain string `json:"terrain" bson:"terrain"`
	NumberMovies int `json:"number_movies" bson:"number_movies"`
	Films []string `json:"films" bson:"films"`
	Url string `json:"url" bson:"url"`
}

type Film struct {
	Title string `json:"title" bson:"title"`
}

var Database string
var Host string
var PathFile string
var data string
var response Response