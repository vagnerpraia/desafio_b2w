package creator_script

import (
	"encoding/json"

	"github.com/globalsign/mgo/bson"
)

func Create () {
	if Host[len(Host)-1:]!= "/" {
		Host = Host + "/"
	}

	data = data + "conn = new Mongo();"
	data = data + "\ndb = conn.getDB(\"" + Database + "\");"
	data = data + "\nvar bulk = db.planet.initializeUnorderedBulkOp();"

	url := "https://swapi.co/api/planets/"
	body := getJson(url)
	json.Unmarshal(body, &response)
	getPage()

	for {
		if response.Next == "" {
			break
		} else {
			body = getJson(response.Next)
			response = Response{}
			json.Unmarshal(body, &response)
			getPage()
		}
	}

	data = data + "\nbulk.execute();"
	writeResult(data)
}

func getPage() {
	for _, planet := range response.Results {
		var films []string
		for _, urlFilm := range planet.Films {
			var film Film
			bodyFilm := getJson(urlFilm)
			json.Unmarshal(bodyFilm, &film)

			films = append(films, film.Title)
		}

		planet.Id = bson.NewObjectId().Hex()
		planet.NumberMovies = len(films)
		planet.Films = films
		planet.Url = Host + "planets/" + planet.Id

		planetJson, _ := json.Marshal(planet)
		jsonInsert := "\nbulk.insert(" + string(planetJson) + ");"
		data = data + jsonInsert
	}
}