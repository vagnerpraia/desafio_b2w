package creator_script

import (
	"log"
)

func printLog(err error) {
	if err != nil {
		log.Println(err)
	}
}