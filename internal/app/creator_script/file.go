package creator_script

import (
	"os"
)

func writeResult(data string) {
	err := os.Remove(PathFile)
	printLog(err)

    f, err := os.OpenFile(PathFile, os.O_CREATE|os.O_WRONLY, 0644)
	printLog(err)

	_, err = f.Write([]byte(data))
	printLog(err)

	err = f.Close()
	printLog(err)
}