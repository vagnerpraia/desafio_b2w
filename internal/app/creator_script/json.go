package creator_script

import (
	"io/ioutil"
	"net/http"
)

func getJson(url string) (body []byte) {
	resp, err := http.Get(url)
	printLog(err)
	defer resp.Body.Close()

	body, err = ioutil.ReadAll(resp.Body)
	printLog(err)

	return
}