package about

import (
	"net/http"

	"gitlab.com/vagnerpraia/desafio_b2w/internal/app/api/models"
)

func GetAbout() (response models.Response) {
	data := getAboutDao()

	response.Code = http.StatusOK
	response.Message = "Informação sobre a aplicação retornada."
	response.Result = data

	return
}