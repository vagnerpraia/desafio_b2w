package about

func getAboutDao() (about About) {
	about = About{
		Nome:   "API do Desafio B2W",
		Versao: "1.0.0",
		Autor:  "Vagner Praia",
		Email:  "vagnerpraia@gmail.com",
	}

	return
}