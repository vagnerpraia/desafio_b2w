package planet

import (
	"net/http"

	"gitlab.com/vagnerpraia/desafio_b2w/internal/app/api/models"
	"gitlab.com/vagnerpraia/desafio_b2w/internal/app/api/utils"
)

func GetPlanets() (response models.Response) {
	planetsResponse, err := getPlanetsDao()

	if err != nil {
		response = utils.HandlerError(err, http.StatusInternalServerError, "Ocorreu um erro.")
	} else {
		if len(planetsResponse) == 0 {
			response.Code = http.StatusNoContent
		} else {
			response.Code = http.StatusOK
			response.Message = "Planetas retornados."
			response.Result = planetsResponse
		}
	}

	return
}

func GetPlanet(id string) (response models.Response) {
	planetResponse, err := getPlanetDao(id)

	if err != nil {
		response = utils.HandlerError(err, http.StatusInternalServerError, "Ocorreu um erro.")
	}
	
	if planetResponse.Id == "" {
		response.Code = http.StatusNoContent
	} else {
		response.Code = http.StatusOK
		response.Message = "Planeta retornado."
		response.Result = planetResponse
	}

	return
}

func PostPlanet(planet Planet) (response models.Response) {
	if planet.Name == "" {
		response.Code = http.StatusForbidden
		response.Message = "Requisição inválida. É necessário a passagem do nome do planeta."
	} else {
		planet.NumberMovies = len(planet.Films)
		planet, err := postPlanetDao(planet)

		if err != nil {
			response = utils.HandlerError(err, http.StatusInternalServerError, "Ocorreu um erro.")
		} else {
			response.Code = http.StatusOK
			response.Message = "Planeta criado."
			response.Result = planet
		}
	}

	return
}

func PutPlanet(planet Planet) (response models.Response) {
	if planet.Name == "" {
		response.Code = http.StatusForbidden
		response.Message = "Requisição inválida. É necessário a passagem do nome do planeta."
	} else {
		planet.NumberMovies = len(planet.Films)
		err := putPlanetDao(planet)

		if err != nil {
			response = utils.HandlerError(err, http.StatusInternalServerError, "Ocorreu um erro.")
		} else {
			response.Code = http.StatusOK
			response.Message = "Planeta atualizado."
			response.Result = planet
		}
	}

	return
}

func DeletePlanet(id string) (response models.Response) {
	err := deletePlanetDao(id)

	if err != nil {
		response = utils.HandlerError(err, http.StatusInternalServerError, "Ocorreu um erro.")
	} else {
		response.Code = http.StatusOK
		response.Message = "Planeta deletado."
	}

	return
}