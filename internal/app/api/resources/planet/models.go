package planet

type Planet struct {
	Id string `json:"id" bson:"_id"`
	Name string `json:"name" bson:"name"`
	Climate string `json:"climate" bson:"climate"`
	Terrain string `json:"terrain" bson:"terrain"`
	NumberMovies int `json:"number_movies" bson:"number_movies"`
	Films []string `json:"films" bson:"films"`
	Url string `json:"url" bson:"url"`
}

type Planets []Planet