package planet

import (
	"github.com/globalsign/mgo/bson"
	"gitlab.com/vagnerpraia/desafio_b2w/internal/app/api/databases"
)

const colletionName = "planet"

var db = databases.GetConnectionMongo()
var collection = db.C(colletionName)

func getPlanetsDao() (planets Planets, err error) {
	err = collection.Find(nil).Sort("name").All(&planets)

	return
}

func getPlanetDao(id string) (planet Planet, err error) {
	err = collection.FindId(id).One(&planet)

	return
}

func postPlanetDao(planetRequest Planet) (planetResponse Planet, err error) {
	if planetRequest.Id == "" {
		planetRequest.Id = bson.NewObjectId().Hex()
	}

	planetRequest.Url = planetRequest.Url + "/" + planetRequest.Id
	err = collection.Insert(&planetRequest)

	planetResponse = planetRequest

	return
}

func putPlanetDao(planet Planet) (err error) {
	err = collection.UpdateId(planet.Id, &planet)

	return
}

func deletePlanetDao(id string) (err error) {
	err = collection.RemoveId(id)

	return
}