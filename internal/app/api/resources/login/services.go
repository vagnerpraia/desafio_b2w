package login

import (
	"net/http"

	"gitlab.com/vagnerpraia/desafio_b2w/internal/app/api/models"
	"gitlab.com/vagnerpraia/desafio_b2w/internal/app/api/utils"
)

func Login(login LoginStruct) (response models.Response) {
	passwordCrypt, err := utils.Crypt(login.Password)
	utils.HandlerError(err, http.StatusInternalServerError, "Ocorreu um erro na criptografia da senha.")

	login.Password = passwordCrypt
	userResponse, err := loginDao(login)

	if err != nil {
		response = utils.HandlerError(err, http.StatusInternalServerError, "Ocorreu um erro.")
	}

	if userResponse.Id == "" {
		response.Code = http.StatusNoContent
	} else {
		response.Code = http.StatusOK
		response.Message = "Login realizado."

		token, err := utils.Crypt(string(userResponse.Type))
		utils.HandlerError(err, http.StatusInternalServerError, "Ocorreu um erro na criptografia do token.")

		response.Result = token
	}

	return
}