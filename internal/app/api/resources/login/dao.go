package login

import (
	"gitlab.com/vagnerpraia/desafio_b2w/internal/app/api/databases"
	"gitlab.com/vagnerpraia/desafio_b2w/internal/app/api/resources/user"
)

const colletionName = "user"

var db = databases.GetConnectionMongo()
var collection = db.C(colletionName)

func loginDao(login LoginStruct) (userResponse user.User, err error) {
	err = collection.Find(login).One(&userResponse)

	return
}