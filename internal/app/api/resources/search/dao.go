package search

import (
	"github.com/globalsign/mgo/bson"
	"gitlab.com/vagnerpraia/desafio_b2w/internal/app/api/databases"
	"gitlab.com/vagnerpraia/desafio_b2w/internal/app/api/resources/planet"
)

const colletionName = "planet"

var db = databases.GetConnectionMongo()
var collection = db.C(colletionName)

func postSearchPlanetDao(search Search) (planets planet.Planets, err error) {
	query := bson.M{
		"$text": bson.M{
			"$search": search.Name,
		},
	}

	sort := bson.M{
		"score": bson.M{
			"$meta": "textScore",
		},
	}

	err = collection.Find(query).Select(sort).Sort("$textScore:score").All(&planets)

	return
}