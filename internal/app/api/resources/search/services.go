package search

import (
	"net/http"

	"gitlab.com/vagnerpraia/desafio_b2w/internal/app/api/models"
	"gitlab.com/vagnerpraia/desafio_b2w/internal/app/api/utils"
)

func PostSearch(search Search) (response models.Response) {
	if search.Name == "" {
		response.Code = http.StatusForbidden
		response.Message = "Requisição inválida. É necessário a passagem do nome do planeta a ser buscado."
	} else {
		planet, err := postSearchPlanetDao(search)

		if err != nil {
			response = utils.HandlerError(err, http.StatusInternalServerError, "Ocorreu um erro.")
		} else {
			if len(planet) == 0 {
				response.Code = http.StatusNoContent
			} else {
				response.Code = http.StatusOK
				response.Message = "Planeta(s) retornado(s)."
				response.Result = planet
			}
		}
	}

	return
}