package user

type typeUser int

type User struct {
	Id string `json:"id" bson:"_id"`
	Name string `json:"name" bson:"name"`
	Email string `json:"email" bson:"email"`
	Password string `json:"password" bson:"password"`
	Type typeUser `json:"type" bson:"type"`
}

const (
	ADMIN typeUser = iota
	PUBLIC typeUser = iota
)