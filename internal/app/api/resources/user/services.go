package user

import (
	"net/http"

	"gitlab.com/vagnerpraia/desafio_b2w/internal/app/api/models"
	"gitlab.com/vagnerpraia/desafio_b2w/internal/app/api/utils"
)

func GetUser(id string) (response models.Response) {
	userResponse, err := getUserDao(id)

	if err != nil {
		response = utils.HandlerError(err, http.StatusInternalServerError, "Ocorreu um erro.")
	}

	if userResponse.Id == "" {
		response.Code = http.StatusNoContent
	} else {
		response.Code = http.StatusOK
		response.Message = "Usuário retornado."
		response.Result = userResponse
	}

	return
}

func PostUser(user User) (response models.Response) {
	if user.Name == "" {
		response.Code = http.StatusForbidden
		response.Message = "Requisição inválida. É necessário a passagem do nome do usuário."
	} else {
		passwordCrypt, err := utils.Crypt(user.Password)
		utils.HandlerError(err, http.StatusInternalServerError, "Ocorreu um erro na criptografia da senha.")

		user.Password = passwordCrypt
		user.Type = PUBLIC

		user, err := postUserDao(user)

		if err != nil {
			response = utils.HandlerError(err, http.StatusInternalServerError, "Ocorreu um erro.")
		} else {
			response.Code = http.StatusOK
			response.Message = "Usuário criado."
			response.Result = user
		}
	}

	return
}

func PutUser(user User) (response models.Response) {
	if user.Name == "" {
		response.Code = http.StatusForbidden
		response.Message = "Requisição inválida. É necessário a passagem do nome do usuário."
	} else {
		passwordCrypt, err := utils.Crypt(user.Password)
		utils.HandlerError(err, http.StatusInternalServerError, "Ocorreu um erro na criptografia da senha.")

		user.Password = passwordCrypt
		user.Type = PUBLIC

		err = putUserDao(user)

		if err != nil {
			response = utils.HandlerError(err, http.StatusInternalServerError, "Ocorreu um erro.")
		} else {
			response.Code = http.StatusOK
			response.Message = "Usuário atualizado."
			response.Result = user
		}
	}

	return
}

func DeleteUser(id string) (response models.Response) {
	err := deleteUserDao(id)

	if err != nil {
		response = utils.HandlerError(err, http.StatusInternalServerError, "Ocorreu um erro.")
	} else {
		response.Code = http.StatusOK
		response.Message = "Usuário deletado."
	}

	return
}

func Login(user User) (response models.Response) {
	userResponse, err := loginDao(user)

	if err != nil {
		response = utils.HandlerError(err, http.StatusInternalServerError, "Ocorreu um erro.")
	}

	if userResponse.Id == "" {
		response.Code = http.StatusNoContent
	} else {
		response.Code = http.StatusOK
		response.Message = "Login realizado."

		token, err := utils.Crypt(string(userResponse.Type))
		utils.HandlerError(err, http.StatusInternalServerError, "Ocorreu um erro na criptografia do token.")

		response.Result = token
	}

	return
}