package user

import (
	"github.com/globalsign/mgo/bson"
	"gitlab.com/vagnerpraia/desafio_b2w/internal/app/api/databases"
)

const colletionName = "user"

var db = databases.GetConnectionMongo()
var collection = db.C(colletionName)

func getUserDao(id string) (user User, err error) {
	err = collection.FindId(id).One(&user)

	return
}

func postUserDao(userRequest User) (userResponse User, err error) {
	if userRequest.Id == "" {
		userRequest.Id = bson.NewObjectId().Hex()
	}

	err = collection.Insert(&userRequest)

	userResponse = userRequest

	return
}

func putUserDao(user User) (err error) {
	err = collection.UpdateId(user.Id, &user)

	return
}

func deleteUserDao(id string) (err error) {
	err = collection.RemoveId(id)

	return
}

func loginDao(userRequest User) (userResponse User, err error) {
	err = collection.Find(userRequest).One(&userResponse)

	return
}