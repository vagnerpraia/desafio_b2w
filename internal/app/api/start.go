package api

import (
	"log"
	"net/http"
	"strconv"

	"gitlab.com/vagnerpraia/desafio_b2w/internal/app/api/routes"
)

var Port int

func Start() {
	router := routes.GetRouter()
	port := strconv.Itoa(Port)

	err := http.ListenAndServe(":" + port, router)
	log.Fatal(err)
}