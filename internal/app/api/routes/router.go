package routes

import (
	"github.com/gorilla/mux"
	"gitlab.com/vagnerpraia/desafio_b2w/internal/app/api/controllers"
	"gitlab.com/vagnerpraia/desafio_b2w/internal/app/api/middlewares"
)

func GetRouter() (router *mux.Router) {
	r := mux.NewRouter().StrictSlash(true)
	router = r.PathPrefix("/v1").Subrouter()

	router.HandleFunc("/", controllers.GetAbout).Methods("GET")
	router.HandleFunc("/about", controllers.GetAbout).Methods("GET")

	router.HandleFunc("/planets", controllers.GetPlanets).Methods("GET")
	router.HandleFunc("/planets/{id}", controllers.GetPlanet).Methods("GET")
	router.HandleFunc("/planets", controllers.PostPlanet).Methods("POST")
	router.HandleFunc("/planets", controllers.PutPlanet).Methods("PUT")
	router.HandleFunc("/planets/{id}", controllers.DeletePlanet).Methods("DELETE")

	router.HandleFunc("/search", controllers.PostSearch).Methods("POST")

	router.HandleFunc("/users/{id}", middlewares.Authenticate(controllers.GetUser)).Methods("GET")
	router.HandleFunc("/users", controllers.PostUser).Methods("POST")
	router.HandleFunc("/users", middlewares.Authenticate(controllers.PutUser)).Methods("PUT")
	router.HandleFunc("/users/{id}", middlewares.Authenticate(controllers.DeleteUser)).Methods("DELETE")

	router.HandleFunc("/login", controllers.Login).Methods("POST")

	return
}