package databases

import (
	"net/http"
	"sync"
	"time"

	"github.com/BurntSushi/toml"
	"github.com/globalsign/mgo"
	"gitlab.com/vagnerpraia/desafio_b2w/internal/app/api/utils"
)

var once sync.Once
var mongo *mgo.Database

func connectMongo() (db *mgo.Database) {
	var config *configMongo
	_, err := toml.DecodeFile("config.toml", &config)
	utils.HandlerError(err, http.StatusInternalServerError, "Ocorreu um erro.")

	timeout := 15 * time.Second

	var session *mgo.Session
	if config.Username == "" && config.Password == "" {
		session, err = mgo.DialWithTimeout(config.Host, time.Duration(timeout))
	} else {
		info := &mgo.DialInfo{
			Addrs:    []string{"mongodb://" + config.Host},
			Timeout:  timeout * time.Second,
			Username: config.Username,
			Password: config.Password,
		}

		session, err = mgo.DialWithInfo(info)
	}
    utils.HandlerFatalError(err)

	db = session.DB(config.Database)

	return
}

func GetConnectionMongo() (*mgo.Database) {
	once.Do(func() {
		mongo = connectMongo()
	})

	return mongo
}