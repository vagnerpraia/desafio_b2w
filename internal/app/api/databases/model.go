package databases

type configMongo struct {
	Host string
	Database string
	Username string
	Password string
}