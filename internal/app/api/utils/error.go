package utils

import (
	"log"

	"gitlab.com/vagnerpraia/desafio_b2w/internal/app/api/models"
)

func HandlerError(err error, code int, message string) (response models.Response) {
	response = models.Response{Code: code, Message: message}

	if err != nil {
		log.Println(err)
	}

	return
}

func HandlerFatalError(err error) {
	if err != nil {
		log.Fatal(err)
	}

	return
}