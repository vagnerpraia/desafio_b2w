package utils

import (
	"encoding/json"
	"net/http"

	"gitlab.com/vagnerpraia/desafio_b2w/internal/app/api/models"
)

func EncodeResponseJson(w http.ResponseWriter, response models.Response) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(response.Code)

	enc := json.NewEncoder(w)
	enc.Encode(response)
}