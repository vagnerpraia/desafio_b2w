package utils

import (
	"golang.org/x/crypto/bcrypt"
)

func Crypt(stringToCrypt string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(stringToCrypt), 14)
	return string(bytes), err
}