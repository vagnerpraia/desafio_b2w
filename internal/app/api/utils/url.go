package utils

import (
	"net/http"
)

func GetFullUrl(r *http.Request) (url string) {
	scheme := "http://"
	if r.TLS != nil {
		scheme = "https://"
	}

	url = scheme + r.Host + r.URL.String()

	return
}

func GetUrl(r *http.Request) (url string) {
	scheme := "http://"
	if r.TLS != nil {
		scheme = "https://"
	}

	url = scheme + r.Host + r.URL.Path

	return
}

func GetUrlWithId(r *http.Request, id string) (url string) {
	scheme := "http://"
	if r.TLS != nil {
		scheme = "https://"
	}

	url = scheme + r.Host + r.URL.Path + "/" + id

	return
}