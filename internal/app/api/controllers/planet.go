package controllers

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/vagnerpraia/desafio_b2w/internal/app/api/resources/planet"
	"gitlab.com/vagnerpraia/desafio_b2w/internal/app/api/utils"
)

func GetPlanets(w http.ResponseWriter, r *http.Request) {
	response := planet.GetPlanets()

	utils.EncodeResponseJson(w, response)
}

func GetPlanet(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id := params["id"]

	response := planet.GetPlanet(id)

	utils.EncodeResponseJson(w, response)
}

func PostPlanet(w http.ResponseWriter, r *http.Request) {
	var planetRequest planet.Planet
	dec := json.NewDecoder(r.Body)
	err := dec.Decode(&planetRequest)
	defer r.Body.Close()

	planetRequest.Url = utils.GetUrl(r)

	if err != nil {
		response := utils.HandlerError(err, http.StatusInternalServerError, "Ocorreu um erro.")
		utils.EncodeResponseJson(w, response)
	} else {
		response := planet.PostPlanet(planetRequest)
		utils.EncodeResponseJson(w, response)
	}
}

func PutPlanet(w http.ResponseWriter, r *http.Request) {
	var planetRequest planet.Planet
	dec := json.NewDecoder(r.Body)
	err := dec.Decode(&planetRequest)
	defer r.Body.Close()

	planetRequest.Url = utils.GetUrlWithId(r, planetRequest.Id)

	if err != nil {
		response := utils.HandlerError(err, http.StatusInternalServerError, "Ocorreu um erro.")
		utils.EncodeResponseJson(w, response)
	} else {
		response := planet.PutPlanet(planetRequest)
		utils.EncodeResponseJson(w, response)
	}
}

func DeletePlanet(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id := params["id"]

	response := planet.DeletePlanet(id)

	utils.EncodeResponseJson(w, response)
}