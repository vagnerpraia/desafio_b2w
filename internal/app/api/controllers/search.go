package controllers

import (
	"encoding/json"
	"net/http"

	"gitlab.com/vagnerpraia/desafio_b2w/internal/app/api/resources/search"
	"gitlab.com/vagnerpraia/desafio_b2w/internal/app/api/utils"
)

func PostSearch(w http.ResponseWriter, r *http.Request) {
	var searchModel search.Search

	dec := json.NewDecoder(r.Body)
	err := dec.Decode(&searchModel)
	if err != nil {
		utils.HandlerError(err, http.StatusInternalServerError, "Ocorreu um erro.")
	}
	defer r.Body.Close()

	response := search.PostSearch(searchModel)

	utils.EncodeResponseJson(w, response)
}