package controllers

import (
	"net/http"

	"gitlab.com/vagnerpraia/desafio_b2w/internal/app/api/resources/about"
	"gitlab.com/vagnerpraia/desafio_b2w/internal/app/api/utils"
)

func GetAbout(w http.ResponseWriter, r *http.Request) {
	response := about.GetAbout()

	utils.EncodeResponseJson(w, response)
}