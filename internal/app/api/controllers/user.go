package controllers

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/vagnerpraia/desafio_b2w/internal/app/api/resources/user"
	"gitlab.com/vagnerpraia/desafio_b2w/internal/app/api/utils"
)

func GetUser(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id := params["id"]

	response := user.GetUser(id)

	utils.EncodeResponseJson(w, response)
}

func PostUser(w http.ResponseWriter, r *http.Request) {
	var userRequest user.User
	dec := json.NewDecoder(r.Body)
	err := dec.Decode(&userRequest)
	defer r.Body.Close()

	if err != nil {
		response := utils.HandlerError(err, http.StatusInternalServerError, "Ocorreu um erro.")
		utils.EncodeResponseJson(w, response)
	} else {
		response := user.PostUser(userRequest)
		utils.EncodeResponseJson(w, response)
	}
}

func PutUser(w http.ResponseWriter, r *http.Request) {
	var userRequest user.User
	dec := json.NewDecoder(r.Body)
	err := dec.Decode(&userRequest)
	defer r.Body.Close()

	if err != nil {
		response := utils.HandlerError(err, http.StatusInternalServerError, "Ocorreu um erro.")
		utils.EncodeResponseJson(w, response)
	} else {
		response := user.PutUser(userRequest)
		utils.EncodeResponseJson(w, response)
	}
}

func DeleteUser(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id := params["id"]

	response := user.DeleteUser(id)

	utils.EncodeResponseJson(w, response)
}