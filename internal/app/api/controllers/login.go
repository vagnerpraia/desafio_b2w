package controllers

import (
	"encoding/json"
	"net/http"

	"gitlab.com/vagnerpraia/desafio_b2w/internal/app/api/resources/login"
	"gitlab.com/vagnerpraia/desafio_b2w/internal/app/api/utils"
)

func Login(w http.ResponseWriter, r *http.Request) {
	var loginRequest login.LoginStruct
	dec := json.NewDecoder(r.Body)
	err := dec.Decode(&loginRequest)
	defer r.Body.Close()

	if err != nil {
		response := utils.HandlerError(err, http.StatusInternalServerError, "Ocorreu um erro.")
		utils.EncodeResponseJson(w, response)
	} else {
		response := login.Login(loginRequest)
		utils.EncodeResponseJson(w, response)
	}
}