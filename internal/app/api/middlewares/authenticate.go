package middlewares

import (
	"net/http"

	"gitlab.com/vagnerpraia/desafio_b2w/internal/app/api/utils"
)

func Authenticate(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		token := r.Header.Get("Authorization")

		if token == "123456" {
			next(w, r)
		} else {
			response := utils.HandlerError(nil, http.StatusForbidden, "Acesso restrito.")
			utils.EncodeResponseJson(w, response)
		}
	}
}