package models

type Response struct {
	Code int `json:"-"`
	Message string `json:"message"`
	Result interface{} `json:"result"`
}