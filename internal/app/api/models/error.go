package models

type Error struct {
	message string
}

type Errors []Error