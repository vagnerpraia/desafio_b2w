A estrutura do projeto segue o padrão apresentado pela iniciativa [Standard Go Project Layout](https://github.com/golang-standards/project-layout). Os diretórios desta estrutura têm a seguinte lógica:

* /bin – contém os arquivos executáveis.
* /cmd – contém os arquivos de código com a inicialização (main) da aplicação, separado em diretórios para cada aplicação.
* /docs – contém arquivos de documentação do projeto.
* /internal – contém os arquivos de código internos da aplicação, separado em diretórios para cada aplicação. Estes arquivos de código, são aqueles que não fazem parte da inicialização da aplicação, que devem ficar no diretório /cmd, e que não são pacotes para serem reutilizados em outros projetos, que devem estar no diretório /pkg.
* /scripts – contém os scripts utilizados no projeto.
* /test – contém os testes do projeto.

A estruturação da API no diretório /internal segue a lógica de separar no diretório /resources a lógica de negócios da aplicação, separando ainda em diretórios para cada área da API. Esta divisão por área atende a separação por recursos de uma API REST. E conceituando, neste diretório é encontrado o Core Domain descrito DDD. Sendo ainda, a divisão por recursos apresenta os Contextos Limitados também do DDD.  

Outra vantagem nesta separação por recursos, facilita caso seja desejado dividir a aplicação em microservices, já que um ponto de partida para isto, é separar cada um desses recursos em um microservice.  

Em Go, os módulos funcionam como uma classe.