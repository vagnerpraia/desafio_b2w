package main

import (
	"flag"
	"fmt"

	creatorScript "gitlab.com/vagnerpraia/desafio_b2w/internal/app/creator_script"
)

func main() {
	database := flag.String("d", "desafio_b2w", "Nome do banco de dados onde os dados serão carregados.")
	host := flag.String("h", "http://127.0.0.1/", "Host do sistema que será utilizada para adicionar na URL dos recursos.")
	pathFile := flag.String("p", "./load_database.js", "Endereço arquivo a ser gerado.")

	flag.Parse()

	creatorScript.Database = *database
	creatorScript.Host = *host
	creatorScript.PathFile = *pathFile
	creatorScript.Create()

	fmt.Println("Script created.")
}