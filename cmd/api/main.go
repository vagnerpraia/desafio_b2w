package main

import (
	"flag"
	"fmt"

	"gitlab.com/vagnerpraia/desafio_b2w/internal/app/api"
)

func main() {
	port := flag.Int("p", 3000, "Porta utilizada para a execução da API.")

	flag.Parse()

	fmt.Println("API Started.")

	api.Port = *port
	api.Start()
}