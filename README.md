# Desafio B2W

API REST para o desafio técnico da B2W.

## Descrição

Este projeto contém a implementação de uma API REST sobre planetas dos filmes de Star Wars, referente a um desafio técnico da B2W. A implementação da API foi realizada em Go e utiliza o banco de dados MongoDB. Além da API, outra aplicação foi desenvolvida para geração de script para o carregamento de dados no banco de dados.  
Na implementação da API, foi utilizado o pacote [gorilla/mux](https://github.com/gorilla/mux) para fazer o roteamento e o pacote [globalsign/mgo](https://github.com/globalsign/mgo) como driver para o MongoDB.   

## Download

| Descrição | Link |
| ------ | ------ |
| API para Windows | [api.exe](https://gitlab.com/vagnerpraia/desafio_b2w/uploads/be823bda9a1abc828b01f1e87428ee3c/api.exe) |
| API para Linux | [api](https://gitlab.com/vagnerpraia/desafio_b2w/uploads/e70c59d9f46608507c8b70b43ee4f0ea/api) |
| creator\_script para Windows | [creator_script.exe](https://gitlab.com/vagnerpraia/desafio_b2w/uploads/f990049f67b4c6d024f23f93f7646416/creator_script.exe) |
| creator\_script para Linux | [creator_script](https://gitlab.com/vagnerpraia/desafio_b2w/uploads/b4d7c47ae743526ae60791390a8653de/creator_script) |

## Dependências

Dependências para a produção:

* MongoDB >= 3.4.6

Dependências para o build:

* Go >= 1.12.1
* Pacote [gorilla/mux](https://github.com/gorilla/mux) = 1.7.2
* Pacote [globalsign/mgo](https://github.com/globalsign/mgo) = r2018.06.15

## Banco de dados

Os arquivos para a criação do banco de dados estão no diretório script, sendo estes:

* create\_database.js - Cria o banco de dados de produção.
* create\_database_test.js - Cria o banco de dados de teste.
* load\_database.js - Carrega no banco de dados os dados buscados na API [SWAPI](https://swapi.co/).
* create\_database.sh - Executa no MongoDB os scripts create\_database.js e load\_database.js.
* create\_database\_test.sh - Executa no MongoDB os scripts create\_database\_test.js e load\_database.js.

Sendo assim, a crição dos bancos de dados (produção e teste) pode ser realizada com o seguinte comando:

```
./scripts/create_database.sh
./scripts/create_database_test.sh
```

## Build

A construção do binário da API pode ser realizada o seguinte comando (Windows e Linux respectivamente):

```
GOOS=windows GOARCH=amd64 CGO_ENABLED=0 go build -a -ldflags '-extldflags "-static" -s -w' -o ./bin/api.exe ./cmd/api/*.go
GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -a -ldflags '-extldflags "-static" -s -w' -o ./bin/api ./cmd/api/*.go
```

## Execução da API

A execução pelo binário é realizada com o seguinte comando (Windows e Linux respectivamente):

```
./bin/api.exe -p {porta}
./bin/api -p {porta}
```

Para a execução direto do código o seguinte comando deve ser utilizado:

```
go run ./cmd/api/main.go -p {porta}
```

## Testes

A execução de todos os testes unitários é realizada com o seguinte comando:

```
go test ./test -v
```

A execução de testes de serviços específicos é realizada da seguinte forma:

```
go test ./test -run TestPostPlanet
go test ./test -run TestGetPlanets
go test ./test -run TestGetPlanet
go test ./test -run TestPutPlanet
go test ./test -run TestDeletePlanet
go test ./test -run TestPostSearch
```

Ainda há a possibilidade de testar utilizando a aplicação [Postman](https://www.getpostman.com/), usando o arquivo desafio_b2w.postman_collection.json do diretório test, que contém testes da API para serem usados nesta aplicação.

## Creator Script

O creator_script é uma aplicação desenvolvida para a geração do arquivo de inserção de dados da aplicação.  
A construção dessa aplicação é realizada com o seguinte comando (Windows e Linux respectivamente):

```
GOOS=windows GOARCH=amd64 CGO_ENABLED=0 go build -a -ldflags '-extldflags "-static" -s -w' -o ./bin/creator_script.exe ./cmd/creator_script/*.go
GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -a -ldflags '-extldflags "-static" -s -w' -o ./bin/creator_script ./cmd/creator_script/*.go
```

A execução pelo código pode ser realizada com o seguinte comando:

```
go run ./cmd/creator_script/main.go -d {nome_banco_dados} -p {endereco_arquivo_criado} -h {host_api}
```

Para a execução pelo binário pode ser realizada pelo comando (Windows e Linux respectivamente):

```
./bin/creator_script.exe -d {nome_banco_dados} -p {endereco_arquivo_criado} -h {host_api}
./bin/creator_script -d {nome_banco_dados} -p {endereco_arquivo_criado} -h {host_api}
```